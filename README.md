# README #

RDHM stand-alone repository for NASA ROSES collaboration.

### What is this repository for? ###

* This will primarily host deck files used to configure and run RDHM over select Colorado River Basins. Other files that are used for set-up and running can also be added.

### Who do I talk to? ###

* paul.micheletty@riverside.com